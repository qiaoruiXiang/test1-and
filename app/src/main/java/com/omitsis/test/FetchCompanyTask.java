package com.omitsis.test;


import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by qiaorui on 20/07/15.
 */


public class FetchCompanyTask extends AsyncTask<Void, Void, String[]> {

    private final String LOG_TAG = FetchCompanyTask.class.getSimpleName();
    private ArrayAdapter<String> mAdapter;


    public FetchCompanyTask(ArrayAdapter<String> adapter) {
        mAdapter = adapter;
    }


    /**
     *
     * Convertir un String JSON a una lista de los nombres companias
     *
     */
    private String[] getCompanyDataFromJson(String companyJsonStr) throws JSONException {

        final String OWM_NAME = "name";
        JSONArray jsonArray = new JSONArray(companyJsonStr); //Ya esta en formato array!
        //Log.v(LOG_TAG, "length:"+jsonArray.length());

        String[] result = new String[jsonArray.length()];

        for (int i=0; i < jsonArray.length(); i++) {
            JSONObject company = jsonArray.getJSONObject(i);
            String name = company.getString(OWM_NAME);
            result[i]=name;
            Log.v(LOG_TAG, i+" : "+name);
        }

        return result;
    }

    @Override
    protected String[] doInBackground(Void... params) {

        //Declaracion de los variables
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String companyJsonStr = null; //String JSON como resultado de download

        try { //Iniciar la coneccion HTTP con URL y download al buffer

            URL url = new URL("http://www.json-generator.com/api/json/get/caZrciQZRu?indent=2");

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            companyJsonStr = buffer.toString();
            Log.v(LOG_TAG, companyJsonStr);

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return null;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }

        try { //analizar JSON
            return getCompanyDataFromJson(companyJsonStr);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }


        return null;
    }


    /**
     *
     * Renovar adaptador con datos nuevos
     */
    @Override
    protected void onPostExecute(String[] result) {
        Log.v(LOG_TAG, "onPostExecute");
        if (result != null) {
            mAdapter.clear();
            for (String companyName : result) {
                mAdapter.add(companyName);
            }
        }
    }
}
