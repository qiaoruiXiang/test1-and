package com.omitsis.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity {

    private final static int MAX_COMPANIES = 20;
    private ArrayAdapter<String> mAdapter;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //Dummy Data
        String[] items = new String[MAX_COMPANIES];
        for (int i = 0; i < MAX_COMPANIES; i++) {
            items[i] = "Company " + i;
        }
        //canviar por List, para que se puede modificar los datos
        List<String> dummyData = new ArrayList<>(Arrays.asList(items));
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                dummyData);

        mListView = (ListView) findViewById(R.id.company_list);
        mListView.setAdapter(mAdapter);



        //Evento cuando clica un elemento
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String companyName = mAdapter.getItem(position);
                //Pasara el nombre de compania como identificador
                Intent intent = new Intent(MainActivity.this, DetailActivity.class)
                        .putExtra(Intent.EXTRA_TEXT, companyName);
                startActivity(intent);
            }
        });

        //Usar un thread para download los datos de companias y renovar el adaptador
        FetchCompanyTask fetchCompanyTask = new FetchCompanyTask(mAdapter);
        fetchCompanyTask.execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
