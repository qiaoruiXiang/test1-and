package com.omitsis.test;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by qiaorui on 20/07/15.
 */
public class DetailActivity extends Activity {

    private ImageView mImageView;
    private TextView mNameView;
    private TextView mIdView;
    private TextView mLongitudeView;
    private TextView mLatitudeView;
    private TextView mAddressView;
    private TextView mEmailView;
    private TextView mDateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Localizar los View's
        mImageView = (ImageView)findViewById(R.id.imageView);
        mNameView = (TextView)findViewById(R.id.name_text);
        mIdView = (TextView)findViewById(R.id.id_text);
        mLongitudeView = (TextView)findViewById(R.id.longitude_text);
        mLatitudeView = (TextView)findViewById(R.id.latitude_text);
        mAddressView = (TextView)findViewById(R.id.address_text);
        mEmailView = (TextView)findViewById(R.id.email_text);
        mDateView = (TextView)findViewById(R.id.date_text);
        

        //Con el identificador de compania, busca informacion detallada y renovar view's
        Intent intent = getIntent();
        String name = intent.getStringExtra(Intent.EXTRA_TEXT);
        FetchCompanyDetailTask fetchCompanyDetailTask = new FetchCompanyDetailTask(name);
        fetchCompanyDetailTask.execute();

    }


    /**
     * Dado el nombre de compania, se renova los view's con informacion de esta compania
     */
    private class FetchCompanyDetailTask extends AsyncTask<Void, Void, JSONObject> {

        private final String LOG_TAG = FetchCompanyDetailTask.class.getSimpleName();
        private String companyName;

        public FetchCompanyDetailTask(String name) {
            companyName = name;
        }


        /**
         *
         * Return el JSON Object del compania que queremos encontrar
         */
        private JSONObject getCompanyData(String companyJsonStr) throws JSONException {

            final String OWM_NAME = "name";

            JSONArray jsonArray = new JSONArray(companyJsonStr);
            //Log.v(LOG_TAG, "length:"+jsonArray.length());

            for (int i=0; i < jsonArray.length(); i++) {
                JSONObject company = jsonArray.getJSONObject(i);
                String name = company.getString(OWM_NAME);
                if (name.equals(companyName)) {
                    Log.v(LOG_TAG,"FOUND!!!!!");
                    return company;
                }
            }
            return null;
        }


        /**
         *
         * Lo mismo con FetchCompanyTask
         */
        @Override
        protected JSONObject doInBackground(Void... params) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            String companyJsonStr = null;

            try {

                URL url = new URL("http://www.json-generator.com/api/json/get/caZrciQZRu?indent=2");

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {

                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {

                    return null;
                }
                companyJsonStr = buffer.toString();


            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
                return null;
            }
            finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            try {
                return getCompanyData(companyJsonStr);
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();
            }

            return null;
        }


        /**
         *
         * Analizar la informacion de JSON y renovar el contenido de view's
         */
        @Override
        protected void onPostExecute(JSONObject company) {

            final String OWM_NAME = "name";
            final String OWM_IMAGEURL = "imageUrl";
            final String OWM_LONGITUDE = "longitude";
            final String OWM_ID = "id";
            final String OWM_LATITUDE = "latitude";
            final String OWM_ADDRESS = "address";
            final String OWM_DATE = "date";
            final String OWM_EMAIL = "email";
            String mImageUrl = "";
            try {
                mNameView.setText(company.getString(OWM_NAME));
                mAddressView.setText("Address: " + company.getString(OWM_ADDRESS));
                mDateView.setText("Date: " + company.getString(OWM_DATE));
                mIdView.setText("ID: " + company.getString(OWM_ID));
                mEmailView.setText("Email: " + company.getString(OWM_EMAIL));
                mLatitudeView.setText("Latitude: " + company.getString(OWM_LATITUDE));
                mLongitudeView.setText("Longitude: " + company.getString(OWM_LONGITUDE));
                mImageUrl = company.getString(OWM_IMAGEURL);
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();
            } catch (Exception e) {
                if (e.getMessage() == null) {
                    Log.e(LOG_TAG, "Download failed");
                } else {

                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }

            //PostExecute esta en MainUI, entonces se puede llamar a otro thread para que descarga
            //imagen
            new DownloadImageTask(mImageView).execute(mImageUrl);
        }
    }


    /**
     * Download un imagen y actualizar ImageView
     */
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String...params) {
            String urlStr = params[0];
            Bitmap img = null;
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(urlStr);
            HttpResponse response;
            try {
                response = (HttpResponse)client.execute(request);
                HttpEntity entity = response.getEntity();
                BufferedHttpEntity bufferedEntity = new BufferedHttpEntity(entity);
                InputStream inputStream = bufferedEntity.getContent();
                img = BitmapFactory.decodeStream(inputStream);
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return img;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                Log.v("image", "download correct");
            }
            bmImage.setImageBitmap(result);
        }
    }


}
